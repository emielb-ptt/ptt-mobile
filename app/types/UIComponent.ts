import { OnInit           } from "@angular/core";
import { RouterExtensions } from "nativescript-angular";

/**
 * @classdesc Base class for Components, with a standard method for navigating backwards.
 * The concrete class must implement the ngOnInit(): void method.
 */
export abstract class UIComponent implements OnInit {
    protected routerExtensions: RouterExtensions;

    abstract ngOnInit(): void;

    /**
     * Go to the previous Page. Unused in the iOS app.
     */
    onNavButtonTap(): void {
        this.routerExtensions.back();
    }
}
