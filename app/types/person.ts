// --> app
import { Activity } from "./activity";

export class Person {
    /**
     * Get a Person without information in them.
     * @returns {Person} a Person with empty strings for first and last name, and phone
     * @constructor
     */
    static EMPTY() {
        return <Person>{ name: { first: "", last: "" }, phone: "" };
    }

    id?: number;
    name: {
        first:   string,
        prefix?: string,
        last:    string,
    };
    phone: string;
    activity?: Activity;

    /**
     * Make a Person from data in an any type object.
     * Copy constructor.
     * @param {any} info the any type definition of a Person
     * @throws PersonError if the definition lacks mandatory properties for a Person
     */
    constructor(info: any) {
        if (!info || !info.name
            || !info.name.first
            || !info.name.last
            || !info.phone) {
            throw new PersonError(`Given object lacks one or more members of a Person.\n\n${JSON.stringify(info)}\n\nNeeds at least name, name.first, name.last and phone`);
        }

        this.id = info.id;
        this.name = {
            first: info.name.first,
            prefix: info.name.prefix,
            last: info.name.last,
        };
        this.phone = info.phone;
        this.activity = info.activity;
    }

    /**
     * @returns {string} the full name of the Person
     */
    toString(): string {
        return this.name.first
            + ( this.name.prefix ? ` ${this.name.prefix} ` : " " )
            + this.name.last;
    }
}

export class PersonError extends TypeError {}
