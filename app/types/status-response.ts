// --> ext/fw
import { Observable } from "rxjs/Observable";
import {isNullOrUndefined} from "utils/types";

export class StatusResponse {
    readonly status:  number;
    readonly id?:     number;
    readonly message: string;
    readonly body?:   any;

    constructor (code: number, text: string, id?: number, body?: any) {
        this.status = code;
        this.message = text;
        this.id = id;
        this.body = body;
    }

    /**
     * Take StatusResponse type errors and pass them on so that all StatusResponse types can be handled in one function,
     * instead of having to treat 4xx and 5xx responses as errors. Rethrow other errors.
     * @param {any} e the received error object
     * @returns {Observable<StatusResponse>} an Observable of encountered StatusResponse type errors
     */
    public static statusErrorFilter(e: any): Observable<StatusResponse> {
        if (e instanceof StatusResponse) {
            return Observable.of(e);
        } else {
            throw e;
        }
    }

    /**
     * Handle the different response codes.
     * Display a dialog on an error response (4xx or 5xx), or return useful information.
     * @param {boolean} rethrow (optional) whether or not to rethrow error statuses, default is true
     * @returns {any} relevant information
     */
    extract(rethrow?: boolean): any {
        switch (this.status) {
            case 200:
                return this.body;
            case 201:
                return this.id;
            case 204:
                return true;
            case 303:
                //TODO: handle?
                break;
            case 400:
                alert({
                    title:        "Invalid definition",
                    message:      this.message,
                    okButtonText: "Go back"
                });
                break;
            case 404:
                alert({
                    title:        "Not found on server",
                    message:      "The entity does not exist on the server.\n\n" +
                                      "Add a new one instead.",
                    okButtonText: "Return"
                });
                break;
            case 409:
                alert({
                    title:        "Not empty",
                    message:      "The activity cannot be removed, because it is not empty.",
                    okButtonText: "Return"
                });
                break;
            case 500:
                alert({
                    title:   "Server error",
                    message: "An error occurred. The server reports:\n\n" +
                                 this.message
                });
                break;
            case 503:
                alert({
                    title:   "Service unavailable",
                    message: this.message
                });
                break;
        }

        if (rethrow || isNullOrUndefined(rethrow)) {
            throw this;
        }
    }
}
