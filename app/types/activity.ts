export class Activity {
    /**
     * Get an Activity without information in it.
     * @returns {Activity} an Activity with empty strings for the ID and name
     * @constructor
     */
    static EMPTY() {
        return <Activity>{id: "", name: ""};
    }

    /**
     * Checks the validity of the ID.
     * A valid ID is a single character string letter: any of the letters a-z, or uppercase A-Z.
     * @param id the ID to check
     * @throws {ActivityError} if the ID is invalid
     */
    static checkID(id: string): void {
        if (!/^[a-zA-Z]$/.test(id)) {
            throw ActivityError.invalidID(id);
        }
    }

    id:   string;
    name: string;
    url?: string;

    constructor(info: any) {
        if (!info) {
            throw new ActivityError(`Not an Activity definition: ${JSON.stringify(info)}`);
        } else {
            let missing: string[] = [];

            if (!info.id) {
                missing.push("ID");
            } else {
                Activity.checkID(info.id);
            }

            if (!info.name) {
                missing.push("name");
            }

            if (missing.length > 0) {
                throw new ActivityError(`Activity definition lacks the following properties: ${missing.join(", ")}`);
            }
        }

        this.id = info.id;
        this.name = info.name;
        this.url = info.url;
    }

    toString = () => this.name;
}

export class ActivityError extends TypeError {
    static invalidID(id: string): ActivityError {
        return new ActivityError(`Invalid ID: '${id}', should be a single letter (case-insensitive).`);
    }
}
