// --> ext/fw
import { Component,
         OnDestroy        } from "@angular/core";
import { RouterExtensions } from "nativescript-angular";
import * as PackageInfo     from "nativescript-appversion";
import { openUrl          } from "utils/utils";

// --> app
import { UIComponent } from "../types/UIComponent";

let SetOrientation = require("nativescript-screen-orientation");

@Component({
    selector:    "ptt-about",
    moduleId:    module.id,
    templateUrl: "about.component.html",
    styleUrls:   [ "about.component.css", "about.component.common.css" ]
})
export class AboutComponent extends UIComponent implements OnDestroy {
    version: string = "";

    constructor(
        protected routerExtensions: RouterExtensions
    ) { super(); }

    ngOnInit(): void {
        SetOrientation.setCurrentOrientation("portrait");
        PackageInfo.getVersionName()
            .then(vn => this.version = vn);
    }

    ngOnDestroy(): void {
        SetOrientation.orientationCleanup();
    }

    /**
     * Open the Apache 2.0 License in the phone's browser.
     */
    openApacheLicense(): void {
        openUrl("https://www.apache.org/licenses/LICENSE-2.0");
    }

    /**
     * Open the MIT License in the phone's browser.
     */
    openMITLicense(): void {
        openUrl("https://opensource.org/licenses/mit-license.php");
    }
}
