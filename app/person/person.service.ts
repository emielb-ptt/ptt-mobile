// --> ext/fw
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

// --> app
import { ActivityServer     } from "../server/services/activity-server.service";
import { ActivityListServer } from "../server/services/activity-ro.service";
import { PersonServer       } from "../server/services/person-server.service";
import { Activity           } from "../types/activity";
import { Person             } from "../types/person";
import { StatusResponse     } from "../types/status-response";

@Injectable()
export class PersonService {
    private activityListServer: ActivityListServer;

    constructor(
        private server: PersonServer,
        actSrv: ActivityServer
    ) {
        this.activityListServer = actSrv;
    }

    /**
     * Add a new Person to the list.
     * @param {Person} p the Person to add
     * @returns {Observable<number>} emits the identifier of the new Person
     */
    addPerson(p: Person): Observable<number> {
        return this.server.addPerson(PersonService.savingCopy(p))
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract());
    }

    /**
     * Get all Activities.
     * @returns {Observable<Activity>} emits each Activity
     */
    getActivities(): Observable<Activity> {
        return this.activityListServer.getActivityList()
            .map(sr => sr.body)
            .flatMap((a: {}[]) => Observable.from(a))
            .map(act => new Activity(act))
    }

    /**
     * Get all Persons. Retries once on failure.
     * @param {string} activity (optional) the Activity to filter on
     * @returns {Observable<Person>} emits each Person
     */
    getPersons(activity?: string): Observable<Person> {
        //caching?
        return this.server.getPersonList(activity)
            .retry(2)
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract())
            .flatMap(p => Observable.from(p))
            .map((p: any) => new Person(p));
    }

    /**
     * Get a Person.
     * @param {number} id the identifier of the Person
     * @returns {Observable<Person>} emits the Person
     */
    getPerson(id: number): Observable<Person> {
        return this.server.getPersonDetails(id)
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract())
            .map((p: any) => new Person(p));
    }

    /**
     * Change the definition of a Person.
     * @param {number} id the identifier of the Person
     * @param {Person} p the new definition of the Person
     * @returns {Observable<boolean>} emits the success of the change
     */
    editPerson(id: number, p: Person): Observable<boolean> {
        return this.server.updatePerson(id, PersonService.savingCopy(p))
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract());
    }

    /**
     * Remove a Person from the list.
     * @param {number} id the identifier of the Person
     * @returns {Observable<boolean>} emits the success of the removal
     */
    removePerson(id: number): Observable<boolean> {
        return this.server.removePerson(id)
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract());
    }

    /**
     * Prepare an any-type deep copy of the Person, which can be sent to the server.
     * @param {Person} p the Person to copy for sending
     * @returns {any} the sendable object
     */
    private static savingCopy(p: Person): any {
        let copy: any = new Person(p);
        copy.activity = copy.activity.toString();
        return copy;
    }
}
