// --> ext/fw
import { Component        } from "@angular/core";
import { ActivatedRoute   } from "@angular/router";
import { RouterExtensions } from "nativescript-angular";
import { confirm          } from "ui/dialogs";
import { isUndefined      } from "utils/types";

// --> app
import { Activity    } from "../../types/activity";
import { Person      } from "../../types/person";
import { UIComponent } from "../../types/UIComponent";

// --> module
import { PersonService } from "../person.service";

@Component({
    selector:    "ptt-person-detail",
    moduleId:    module.id,
    templateUrl: "person-detail.component.html"
})
export class PersonDetailComponent extends UIComponent {
    person: Person;

    addMode  = false;
    editMode = false;

    activities: Activity[];
    selectedActivity = 0;

    constructor(
        private personService: PersonService,
        private route: ActivatedRoute,
        protected routerExtensions: RouterExtensions
    ) { super(); }

    /**
     * Load the Person's details.
     */
    ngOnInit(): void {
        this.addMode = isUndefined(this.route.snapshot.params["id"]);
        if (this.addMode) {
            this.person = Person.EMPTY();
        } else {
            this.editMode = this.route.snapshot.url.slice(-1)[0].toString() === "edit";
            this.refresh();
        }

        // load Activities for the list if in addMode or editMode
        if (this.addMode || this.editMode) {
            this.activities = [{id: "0", name: "loading"}, {id: "0", name: "please wait"}];

            let loading: Activity[] = [];
            this.personService.getActivities()
                .subscribe({
                    next: act => loading.push(act),
                    error: () => this.activities[1].name = "failed",
                    complete: () => this.activities = loading
                });
        }
    }

    /**
     * Request the details of a Person from the server and check which mode.
     * The Person to display is determined by the 'id' path parameter.
     */
    refresh(): void {
        if (this.addMode) {
            return;
        }

        const id = +this.route.snapshot.params["id"];

        let backup = this.person;

        this.personService.getPerson(id).subscribe({
            next: (p: Person) => {
                this.person = p;
                this.selectedActivity = this.activities.indexOf(p.activity);

                if (this.selectedActivity === -1) {
                    this.selectedActivity = 0;
                }
            },
            error: err => {
                this.person = backup;

                if (err.status === 404 || err.status === 503) {
                    this.routerExtensions.back();
                }
            }
        });
    }

    /**
     * Go to the edit Page for this Person.
     */
    onEditButtonTap(): void {
        this.routerExtensions.navigateByUrl(this.route.snapshot.url.join('/') + "/edit");
    }

    /**
     * Remove the Person and go back to the previous Page, after confirmation.
     */
    onDeleteButtonTap(): void {
        confirm(`Are you sure you want to remove ${this.person}?`)
            .then(remove => {
                if (remove) {
                    this.personService.removePerson(+this.route.snapshot.params["id"])
                        .subscribe({
                            next: (successful: boolean) => {
                                if (successful) {
                                    this.routerExtensions.back();
                                    //TODO: should then refresh() the navigated-to Page
                                }
                            }
                        });
                }
            });
    }

    /**
     * Save the changes to this Person and go back to the previous Page.
     */
    onSaveButtonTap(): void {
        let id = +this.route.snapshot.params["id"];
        this.person.activity = this.activities[this.selectedActivity];

        this.personService.editPerson(id, this.person)
            .subscribe({
                next: (successful: boolean) => {
                    if (successful) {
                        this.routerExtensions.back();
                        //TODO: should then refresh() the navigated-to Page
                    }
                }
            });
    }

    /**
     * Add the Person and go to their details Page.
     */
    onAddButtonTap(): void {
        this.person.activity = this.activities[this.selectedActivity];

        this.personService.addPerson(this.person)
            .subscribe({
                next: (id: number) => {
                    this.routerExtensions.navigateByUrl(`/person/id/${id}`);
                }
            });
    }
}
