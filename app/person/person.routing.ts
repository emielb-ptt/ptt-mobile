// --> ext/fw
import { Routes } from "@angular/router";

// --> module
import { PersonComponent       } from "./person.component";
import { PersonDetailComponent } from "./detail/person-detail.component";

export const personRoutes: Routes = [
    {
        path: "person",
        component: PersonComponent
    },
    {
        path: "person/act/:id",
        component: PersonComponent
    },
    {
        path: "person/add",
        component: PersonDetailComponent
    },
    {
        path: "person/id/:id",
        component: PersonDetailComponent
    },
    {
        path: "person/id/:id/edit",
        component: PersonDetailComponent
    }
];
