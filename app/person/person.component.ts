// --> ext/fw
import { Component        } from "@angular/core";
import { ActivatedRoute   } from "@angular/router";
import { RouterExtensions } from "nativescript-angular";
import { PullToRefresh    } from "nativescript-pulltorefresh";

// --> app
import { Person      } from "../types/person";
import { UIComponent } from "../types/UIComponent";

// --> module
import { PersonService } from "./person.service";

@Component({
    selector:    "ptt-person",
    moduleId:    module.id,
    templateUrl: "person.component.html"
})
export class PersonComponent extends UIComponent {
    persons:  Person[];
    loading:  Person[];
    activity: string;

    constructor(
        private personService:      PersonService,
        private route:              ActivatedRoute,
        protected routerExtensions: RouterExtensions
    ) { super(); }

    ngOnInit(): void {
        this.activity = this.route.snapshot.params["id"];

        this.refresh();
    }

    /**
     * Request the list of Persons from the server.
     * Display an alert when an error occurs.
     */
    refresh(event?: any): void {
        this.loading = [];
        let pullList: PullToRefresh;
        if (event) {
            pullList = <PullToRefresh>event.object;
        }

        this.personService.getPersons(this.activity)
            .subscribe({
                next: (p: Person) => this.loading.push(p),
                error: err => {
                    this.loading = undefined;
                    if (pullList) {
                        pullList.refreshing = false;
                    }
                },
                complete: () => {
                    this.persons = this.loading.sort();
                    this.loading = undefined;
                    if (pullList) {
                        pullList.refreshing = false;
                    }
                }
            });
    }
}
