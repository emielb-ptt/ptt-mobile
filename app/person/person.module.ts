// --> ext/fw
import { NgModule                 } from "@angular/core";
import { NativeScriptFormsModule,
         NativeScriptModule,
         NativeScriptRouterModule } from "nativescript-angular";

// --> app
import { ServerModule } from "../server/server.module";

// --> module
import { PersonComponent       } from "./person.component";
import { PersonDetailComponent } from "./detail/person-detail.component";
import { personRoutes          } from "./person.routing";
import { PersonService         } from "./person.service";

@NgModule({
    imports: [
        NativeScriptFormsModule,
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(personRoutes),

        ServerModule
    ],
    declarations: [
        PersonComponent,
        PersonDetailComponent
    ],
    providers: [
        PersonService
    ],
    exports: [
        PersonComponent,
        PersonDetailComponent
    ]
})
export class PersonModule { }
