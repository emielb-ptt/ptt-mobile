// --> ext/fw
import { NgModule                 } from "@angular/core";
import { NativeScriptFormsModule,
         NativeScriptHttpModule,
         NativeScriptModule,
         NativeScriptRouterModule } from "nativescript-angular";

// --> module
import { ActivityServer  } from "./services/activity-server.service";
import { PersonServer    } from "./services/person-server.service";
import { ServerComponent } from "./server.component";
import { serverRoutes    } from "./server.routing";
import { ServerService   } from "./server.service";

@NgModule({
    imports: [
        NativeScriptFormsModule,
        NativeScriptHttpModule,
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(serverRoutes)
    ],
    declarations: [
        ServerComponent
    ],
    providers: [
        ActivityServer,
        PersonServer,
        ServerService
    ],
    exports: [
        ServerComponent
    ]
})
export class ServerModule { }
