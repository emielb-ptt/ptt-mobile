// --> ext/fw
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

// --> app
import { StatusResponse } from "../../types/status-response";

// --> module
import { ServerService      } from "../server.service";
import { ActivityListServer } from "./activity-ro.service";

@Injectable()
export class ActivityServer implements ActivityListServer {
    constructor (
        private serverService: ServerService
    ) { }

    /**
     * Ask the server for a list of all activities.
     * @returns {Observable<StatusResponse>} emits the StatusResponse of getting the activity list
     */
    getActivityList(): Observable<StatusResponse> {
        return this.serverService.get("/activity/");
    }

    /**
     * Ask the server for the details of an activity.
     * @param {string} id the id of the activity
     * @returns {Observable<StatusResponse>} emits the StatusResponse of getting a specific activity
     */
    getActivityDetails(id: string): Observable<StatusResponse> {
        return this.serverService.get(`/activity/${id}`);
    }

    /**
     * Set the details of an activity.
     * @param {string} id the id of the activity
     * @param {any} activity the new details of the activity
     * @returns {Observable<StatusResponse>} emits the StatusResponse of setting
     */
    setActivity(id: string, activity: any): Observable<StatusResponse> {
        return this.serverService.put(`/activity/${id}`, activity);
    }

    /**
     * Remove an activity from the server.
     * @param {string} id the id of the activity
     * @returns {Observable<StatusResponse>} emits the StatusResponse of removing
     */
    removeActivity(id: string): Observable<StatusResponse> {
        return this.serverService.delete(`/activity/${id}`);
    }

    /**
     * Ask the server for a list of all persons in an activity.
     * @param {string} id the id of the activity
     * @returns {Observable<StatusResponse>} emits the StatusResponse of getting
     */
    getActivityPersonList(id: string): Observable<StatusResponse> {
        return this.serverService.get(`/activity/${id}/person`);
    }

    /**
     * Ask the server for the URL for an activity.
     * @param {string} id the id of the activity
     * @returns {Observable<StatusResponse>} emits the redirecting StatusResponse
     */
    getActivityURL(id: string): Observable<StatusResponse> {
        return this.serverService.get(`/activity/${id}/url`);
    }
}