// --> ext/fw
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

// --> app
import { StatusResponse } from "../../types/status-response";

// --> module
import { ServerService } from "../server.service";

@Injectable()
export class PersonServer {
    constructor (
        private serverService: ServerService
    ) { }

    /**
     * Add a person to the server.
     * @param {any} person the details for the new person
     * @returns {Observable<StatusResponse>} emits the StatusResponse of adding
     */
    addPerson(person: any): Observable<StatusResponse> {
        return this.serverService.post("/person/", person);
    }

    /**
     * Ask the server for a list of all persons.
     * @param {string} activity (optional) only retrieve persons in the activity with this id
     * @returns {Observable<StatusResponse>} emits the StatusResponse of getting the person list
     */
    getPersonList(activity?: string): Observable<StatusResponse> {
        if (activity) {
            return this.serverService.get(`/activity/${activity}/person`);
        } else {
            return this.serverService.get("/person/");
        }
    }

    /**
     * Ask the server for the details of a person.
     * @param {number} id the id of the person
     * @returns {Observable<StatusResponse>} emits the StatusResponse of getting a specific person
     */
    getPersonDetails(id: number): Observable<StatusResponse> {
        return this.serverService.get(`/person/${id}`);
    }

    /**
     * Update the details of a person.
     * @param {number} id the id of the person
     * @param {any} person the new details of the person
     * @returns {Observable<StatusResponse>} emits the StatusResponse of updating
     */
    updatePerson(id: number, person: any): Observable<StatusResponse> {
        return this.serverService.put(`/person/${id}`, person);
    }

    /**
     * Remove a person from the server.
     * @param {number} id the id of the person
     * @returns {Observable<StatusResponse>} emits the StatusResponse of removing
     */
    removePerson(id: number): Observable<StatusResponse> {
        return this.serverService.delete(`/person/${id}`);
    }
}
