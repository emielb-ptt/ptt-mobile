// --> ext/fw
import { Observable } from "rxjs/Observable";

// --> app
import { StatusResponse } from "../../types/status-response";

export interface ActivityListServer {
    /**
     * Ask the server for a list of all activities.
     * @returns {Observable<StatusResponse>} emits the StatusResponse of getting the activity list
     */
    getActivityList(): Observable<StatusResponse>;
}