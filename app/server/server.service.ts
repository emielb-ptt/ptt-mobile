// --> ext/fw
import { Injectable       } from "@angular/core";
import { Headers,
         Http,
         Response,
         RequestMethod    } from "@angular/http";
import * as Settings        from "application-settings";
import { RouterExtensions } from "nativescript-angular";
import { Observable       } from "rxjs";
import { alert            } from "ui/dialogs";

// --> app
import { StatusResponse } from "../types/status-response";

@Injectable()
export class ServerService {
    private static readonly ADDRESS_KEY = "server-address";
    private static readonly CONTENT_JSON_HEADER = new Headers({ "Content-Type": "application/json" });

    private static readonly STATUS_UNAVAILABLE = new StatusResponse(503,"The server could not be connected to. Check your connection, please.");
    private static readonly STATUS_TIMEOUT = new StatusResponse(503,"The server took too long to respond. Try again later.");

    private static LAST_CHECKED: number;
    private static CNX_CHECK_INTV = 600000; // 10 minutes

    constructor (
        private http: Http,
        private routerExtensions: RouterExtensions
    ) { }

    /**
     * Retrieve the server address from application settings.
     * If it is not found there, return an empty string.
     * @returns {string} the server address
     */
    static getAddress(): string {
        return Settings.getString(ServerService.ADDRESS_KEY, "");
    }

    /**
     * Save the server address in the application's settings.
     * @param {string} address the server address
     * @returns {boolean} true if the address was saved successfully
     */
    static storeAddress(address: string): boolean {
        console.log(`Storing server address ${address} in settings.`);
        Settings.setString(ServerService.ADDRESS_KEY, address);
        return true;
    }

    /**
     * Send a POST request to the registered server.
     * @param {string} endpoint the endpoint of the server to send the request to
     * @param {any} content the body object of the request, will be stringified
     * @returns {Observable<StatusResponse>} emits the StatusResponse of the POST request
     */
    post(endpoint: string, content: any): Observable<StatusResponse> {
        return this.checkURL(endpoint)
            .flatMap(url => {
                return this._post(url, JSON.stringify(content))
                    .map(res => new StatusResponse(res.status, res.json().message, res.json().id))
            });
    }

    /**
     * Send a GET request to the registered server.
     * @param {string} endpoint the endpoint of the server to send the request to
     * @returns {Observable<StatusResponse>} emits the StatusResponse of the GET request
     */
    get(endpoint: string): Observable<StatusResponse> {
        return this.checkURL(endpoint)
            .flatMap(url => {
                return this._get(url)
                    .map(res => new StatusResponse(res.status, "", null, res.json()));
            });
    }

    /**
     * Send a PUT request to the registered server.
     * @param {string} endpoint the endpoint of the server to send the request to
     * @param {any} content the body object of the request, will be stringified
     * @returns {Observable<StatusResponse>} emits the StatusResponse of the PUT request
     */
    put(endpoint: string, content: any): Observable<StatusResponse> {
        return this.checkURL(endpoint)
            .flatMap(url => {
                return this._put(url, JSON.stringify(content))
                    .map(res => new StatusResponse(res.status, res.text()))
            });
    }

    /**
     * Send a DELETE request to the registered server.
     * @param {string} endpoint the endpoint of the server to send the request to
     * @returns {Observable<StatusResponse>} emits the StatusResponse of the DELETE request
     */
    delete(endpoint: string): Observable<StatusResponse> {
        return this.checkURL(endpoint)
            .flatMap(url => {
                return this._delete(url)
                    .map(res => new StatusResponse(res.status, res.text()))
            });
    }

    /**
     * Check the connection to a ptt-server.
     * If no address is given, it is read from the phone's settings.
     * @param {string} address (optional) the address to the ptt-server
     * @returns {Observable<boolean>} emits whether the check passed
     */
    checkConnection(address?: string): Observable<boolean> {
        if (!address) {
            return this.checkConnection(ServerService.getAddress())
        } else {
            console.log("Checking server connection with " + address);
            return this.http.get(address)
                .timeout(3000)
                .map(res => res.json())
                .map(body => {
                    return body.message === "Hello World!"
                        && body.name === "ptt-server"
                        && body.version
                        && body.date;
                })
                .map(check => {
                    if (check) {
                        ServerService.LAST_CHECKED = Date.now();
                    } else {
                        ServerService.LAST_CHECKED = null;
                    }

                    return check;
                })
                .catch(() => Observable.of(false));
        }
    }

    /**
     * Get the server address from the application's settings and combine with the endpoint given.
     * @param {string} endpoint the server endpoint
     * @returns {Observable<string>} emits the endpoint's full address or an error if the server address is missing
     */
    private checkURL(endpoint: string): Observable<string> {
        let address = Observable.of(ServerService.getAddress() + endpoint);

        if (ServerService.LAST_CHECKED && ServerService.LAST_CHECKED + ServerService.CNX_CHECK_INTV > Date.now()) {
            return address;
        } else if (!Settings.hasKey(ServerService.ADDRESS_KEY)) {
            alert("No server address is given. Please enter it in the settings page, which will now open.")
                .then(() => this.routerExtensions.navigateByUrl("server"));
            return Observable.empty();
        } else {
            return this.checkConnection()
                .flatMap(check => {
                    if (check) {
                        return address;
                    } else {
                        throw ServerService.STATUS_UNAVAILABLE;
                    }
                });
        }
    }

    private _post(url: string, body: any): Observable<Response> {
        return this._req(RequestMethod.Post, url, body, ServerService.CONTENT_JSON_HEADER);
    }

    private _get(url: string): Observable<Response> {
        return this._req(RequestMethod.Get, url);
    }

    private _put(url: string, body: any): Observable<Response> {
        return this._req(RequestMethod.Put, url, body, ServerService.CONTENT_JSON_HEADER);
    }

    private _delete(url: string): Observable<Response> {
        return this._req(RequestMethod.Delete, url);
    }

    private _req(method: RequestMethod, url: string, body?: any, headers?: Headers): Observable<Response> {
        return this.http.request(url, {
                method: method,
                headers: headers,
                body: body
            })
            .timeoutWith(3000, Observable.throw(ServerService.STATUS_TIMEOUT))
            .catch(err => {
                if (err instanceof StatusResponse) {
                    throw err;
                } else if (err.status === 200 && !err.url) {
                    throw ServerService.STATUS_UNAVAILABLE;
                } else {
                    throw new StatusResponse(err.status, err.json().message);
                }
            });
    }
}
