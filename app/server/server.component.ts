// --> ext/fw
import { Component        } from "@angular/core";
import { RouterExtensions } from "nativescript-angular";
import { isUndefined      } from "utils/types";

// --> app
import { UIComponent } from "../types/UIComponent";

// --> module
import { ServerService } from "./server.service";

@Component({
    selector: "ptt-server",
    moduleId: module.id,
    templateUrl: "server.component.html",
    styleUrls: [ "server.component.css" ]
})
export class ServerComponent extends UIComponent {
    serverAddress = "";
    enterAddress: string;

    isProcessing = false;
    failedCheck = false;
    passedCheck = false;

    constructor(
        private serverService: ServerService,
        protected routerExtensions: RouterExtensions
    ) { super(); }

    /**
     * Retrieve the server address.
     */
    ngOnInit() {
        this.serverAddress = ServerService.getAddress();

        if (!this.serverAddress) {
            this.enterAddress = "http://192.168.1.107:3000";
        } else {
            this.enterAddress = this.serverAddress;
        }
    }

    /**
     * Test whether the entered address leads to a responding server.
     * Indicate the success or failure to the user by colouring the entered address
     * green on success or red on failure. Also display a message if the connection fails.
     */
    testConnection(): void {
        console.log("Testing connection.");
        this.isProcessing = true;

        this.formatAddress();

        this.serverService.checkConnection(this.enterAddress)
            .subscribe((check) => {
                this.setConnectionChecks(check);
                this.isProcessing = false;
            });
    }

    /**
     * Test the connection as with testConnection(),
     * then save the server address if the check passes.
     */
    saveConnection(): void {
        this.serverService.checkConnection(this.enterAddress)
            .subscribe((check) => {
                this.setConnectionChecks(check);

                if (check) {
                    if (ServerService.storeAddress(this.enterAddress)) {
                        this.serverAddress = this.enterAddress;
                    } else {
                        alert("An error occurred while trying to save the server address.");
                    }
                }

                this.isProcessing = false;
            });
    }

    /**
     * Set the booleans which toggle UI elements indicating
     * the success or failure of connection checks.
     * @param {boolean} hasPassed whether the check has passed
     */
    private setConnectionChecks(hasPassed?: boolean): void {
        this.passedCheck = hasPassed;
        this.failedCheck = !(isUndefined(hasPassed)||hasPassed);
    }

    /**
     * Format the address in the entering field.
     */
    private formatAddress(): void {
        this.enterAddress = "http://" +
            this.enterAddress
            .trim()
            .replace(/^(http|https):\/\//, "")
            .replace(/\/$/, "");
    }
}
