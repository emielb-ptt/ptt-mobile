// --> ext/fw
import { Routes } from "@angular/router";

// --> module
import { ServerComponent } from "./server.component";

export const serverRoutes: Routes = [
    {
        path: "server",
        component: ServerComponent
    }
];
