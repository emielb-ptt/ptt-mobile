// --> ext/fw
import { Routes } from '@angular/router';

// --> module
import { AboutComponent } from "./about/about.component";
import { MenuComponent  } from "./menu/menu.component";

export const appRoutes: Routes = [
    {
        path: "",
        redirectTo: "/menu",
        pathMatch: "full"
    },
    {
        path: "menu",
        component: MenuComponent
    },
    {
        path: "about",
        component: AboutComponent
    }
];
