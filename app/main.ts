// --> ext/fw
    // this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
    // for registering HTML-like elements e.g. from plugins
import { registerElement } from "nativescript-angular/element-registry";
    // enables the detection of screen orientation for use in CSS selection
import "nativescript-orientation";

// --> app
import { AppModule } from "./app.module";

// register plugin element
registerElement("pullToRefresh",() => require("nativescript-pulltorefresh").PullToRefresh);

platformNativeScriptDynamic().bootstrapModule(AppModule);
