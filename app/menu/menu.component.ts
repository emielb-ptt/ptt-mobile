// --> ext/fw
import { Component,
         OnInit } from "@angular/core";
import { Page   } from "ui/page";

@Component({
    selector:    "ptt-menu",
    moduleId:    module.id,
    templateUrl: "menu.component.html",
    styleUrls: [ "menu.component.css", "menu.component.common.css" ]
})
export class MenuComponent implements OnInit {
    constructor(private page: Page) { }

    ngOnInit() {
        // do not display the action bar in the main menu
        this.page.actionBarHidden = true;
    }
}
