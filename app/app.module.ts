// --> ext/fw
import { NgModule                 } from "@angular/core";
import { NativeScriptModule,
         NativeScriptRouterModule } from "nativescript-angular";

// --> app
import { ActivityModule } from "./activity/activity.module";
import { PersonModule   } from "./person/person.module";
import { ServerModule   } from "./server/server.module";

// --> module
import { AboutComponent } from "./about/about.component";
import { AppComponent   } from "./app.component";
import { appRoutes      } from "./app.routing";
import { MenuComponent  } from "./menu/menu.component";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(appRoutes),

        ActivityModule,
        PersonModule,
        ServerModule,
    ],
    declarations: [
        AboutComponent,
        AppComponent,
        MenuComponent
    ]
})
export class AppModule { }
