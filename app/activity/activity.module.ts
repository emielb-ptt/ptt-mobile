// --> ext/fw
import { NgModule                 } from "@angular/core";
import { NativeScriptFormsModule,
         NativeScriptModule,
         NativeScriptRouterModule } from "nativescript-angular";

// --> app
import { ServerModule } from "../server/server.module";

// --> module
import { ActivityComponent       } from "./activity.component";
import { ActivityDetailComponent } from "./detail/activity-detail.component";
import { activityRoutes          } from "./activity.routing";
import { ActivityService         } from "./activity.service";

@NgModule({
    imports: [
        NativeScriptFormsModule,
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(activityRoutes),

        ServerModule
    ],
    declarations: [
        ActivityComponent,
        ActivityDetailComponent
    ],
    providers: [
        ActivityService
    ],
    exports: [
        ActivityComponent,
        ActivityDetailComponent
    ]
})
export class ActivityModule { }