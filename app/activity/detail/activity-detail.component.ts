// --> ext/fw
import { Component        } from "@angular/core";
import { ActivatedRoute   } from "@angular/router";
import { RouterExtensions } from "nativescript-angular";
import { confirm          } from "ui/dialogs";
import { isUndefined      } from "utils/types";

// --> app
import { Activity       } from "../../types/activity";
import { StatusResponse } from "../../types/status-response";
import { UIComponent    } from "../../types/UIComponent";

// --> module
import { ActivityService } from "../activity.service";

@Component({
    selector: "ptt-activity-detail",
    moduleId: module.id,
    templateUrl: "activity-detail.component.html"
})
export class ActivityDetailComponent extends UIComponent {
    activity: Activity;

    addMode  = false;
    editMode = false;

    constructor(
        private activityService: ActivityService,
        private route: ActivatedRoute,
        protected routerExtensions: RouterExtensions
    ) { super(); }

    /**
     * Load the Activity's details.
     */
    ngOnInit(): void {
        this.addMode = isUndefined(this.route.snapshot.params["id"]);

        if (this.addMode) {
            this.activity = Activity.EMPTY();
        } else {
            this.editMode = this.route.snapshot.url.slice(-1)[0].toString() === "edit";
            this.refresh();
        }
    }

    /**
     * Request the details of an Activity from the server and check which mode.
     * The Activity to display is determined by the 'id' path parameter.
     */
    refresh(): void {
        if (this.addMode) {
            return;
        }

        const id = this.route.snapshot.params["id"];

        let backup = this.activity;

        this.activityService.getActivity(id).subscribe({
            next: (act: Activity) => this.activity = act,
            error: err => {
                this.activity = backup;

                if (err.status === 404 || err.status === 503) {
                    this.routerExtensions.back();
                }
            }
        });
    }

    /**
     * Remove the Activity and go back to the previous Page, after confirmation.
     */
    onDeleteButtonTap(): void {
        confirm(`Are you sure you want to remove ${this.activity.name}?`)
            .then(remove => {
                if (remove) {
                    this.activityService.removeActivity(this.route.snapshot.params["id"])
                        .subscribe({
                            next: (successful: boolean) => {
                                if (successful) {
                                    this.routerExtensions.back();
                                    //TODO: should then refresh() the navigated-to Page
                                }
                            }
                        });
                }
            });
    }

    /**
     * Save the changes to the Activity and go back to the previous Page.
     */
    onSaveButtonTap(): void {
        let id = this.route.snapshot.params["id"];

        this.activityService.setActivity(id, this.activity)
            .subscribe({
                next: (successful: boolean) => {
                    if (successful) {
                        this.routerExtensions.back();
                        //TODO: should then refresh() the navigated-to Page
                    }
                }
            });
   }

    /**
     * Add the Activity and go to its details Page.
     */
    onAddButtonTap(): void {
        this.activity.id = this.activity.id.toUpperCase();
        let act: Activity;

        try {
            act = new Activity(this.activity);
        } catch (err) {
            let sr = new StatusResponse(400, `${err}`);
            sr.extract(false);
            return;
        }

        this.activityService.addActivity(act)
            .subscribe({
                next: (successful: boolean) => {
                    if (successful) {
                        this.routerExtensions.navigateByUrl(`/activity/id/${this.activity.id}`);
                    }
                }
            });
    }
}
