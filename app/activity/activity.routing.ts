// --> ext/fw
import { Routes } from "@angular/router";

// --> module
import { ActivityComponent       } from "./activity.component";
import { ActivityDetailComponent } from "./detail/activity-detail.component";

export const activityRoutes: Routes = [
    {
        path: "activity",
        component: ActivityComponent
    },
    {
        path: "activity/add",
        component: ActivityDetailComponent
    },
    {
        path: "activity/id/:id",
        component: ActivityDetailComponent
    },
    {
        path: "activity/id/:id/edit",
        component: ActivityDetailComponent
    }
];
