// --> ext/fw
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { alert,
         confirm    } from "ui/dialogs";

// --> app
import { ActivityServer } from "../server/services/activity-server.service";
import { Activity       } from "../types/activity";
import { StatusResponse } from "../types/status-response";

// --> module

@Injectable()
export class ActivityService {
    constructor(
        private server: ActivityServer
    ) { }

    /**
     * Add a new Activity to the list, if the ID is unique.
     * Otherwise, ask whether to return to change the ID
     * or update the existing Activity.
     * @param {Activity} act the Activity to add
     * @returns {Observable<boolean>} the success of adding
     */
    addActivity(act: Activity): Observable<boolean> {
        try {
            act = new Activity(act);
        } catch (err) {
            return new StatusResponse(400, err.message)
                .extract()
                .map(() => false);
        }

        return this.server.getActivityDetails(act.id)
            .catch(StatusResponse.statusErrorFilter)
            .flatMap((sr: StatusResponse) => {
                switch (sr.status) {
                    case 200:
                        return Observable.fromPromise(
                            confirm({
                                title:   "ID exists",
                                message: `The identifier '${act.id}' is already in use by activity '${sr.body.name}'\n\n` +
                                             "Change the identifier value, or update the existing activity.",
                                okButtonText:     "Update",
                                cancelButtonText: "Change"
                            }))
                            .flatMap(doUpdate => {
                                if (doUpdate) {
                                    return this.setActivity(act.id, act);
                                } else {
                                    return Observable.of(false);
                                }
                            });
                    case 404:
                        return this.setActivity(act.id, act);
                    default:
                        return sr.extract();
                }
            });
    }

    /**
     * Get all Activities.
     * @returns {Observable<Activity>} emits each Activity
     */
    getActivities(): Observable<Activity> {
        return this.server.getActivityList()
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract())
            .flatMap((a: {}[]) => Observable.from(a))
            .map(act => new Activity(act));
    }

    /**
     * Get an Activity.
     * @param {string} id the identifier of the Activity
     * @returns {Observable<Activity>} emits the Activity
     */
    getActivity(id: string): Observable<Activity> {
        return this.server.getActivityDetails(id)
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract())
            .map(act => new Activity(act));
    }

    /**
     * Set the definition of an Activity.
     * @param {string} id the identifier of the Activity
     * @param {Activity} act the new definition of the Activity
     * @returns {Observable<boolean>} emits the success of the change
     */
    setActivity(id: string, act: Activity): Observable<boolean> {
        return this.server.setActivity(id, act)
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract());
    }

    /**
     * Remove an Activity from the list.
     * @param {string} id the identifier of the Activity
     * @returns {Observable<boolean>} emits the success of the removal
     */
    removeActivity(id: string): Observable<boolean> {
        return this.server.removeActivity(id)
            .catch(StatusResponse.statusErrorFilter)
            .map(sr => sr.extract());
    }
}
