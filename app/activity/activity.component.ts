// --> ext/fw
import { Component        } from "@angular/core";
import { RouterExtensions } from "nativescript-angular";
import { PullToRefresh    } from "nativescript-pulltorefresh";
import { openUrl          } from "utils/utils";

// --> app
import { ServerService } from "../server/server.service";
import { Activity      } from "../types/activity";
import { UIComponent   } from "../types/UIComponent";

// --> module
import { ActivityService } from "./activity.service";

@Component({
    selector:    "ptt-activity",
    moduleId:    module.id,
    templateUrl: "activity.component.html",
    styleUrls: [ "activity.component.common.css", "activity.component.css" ]
})
export class ActivityComponent extends UIComponent {
    activities: Activity[];
    loading:    Activity[];

    constructor(
        private activityService:  ActivityService,
        protected routerExtensions: RouterExtensions
    ) { super(); }

    ngOnInit(): void {
        this.refresh();
    }

    /**
     * Request the list of Activities from the server.
     * Display an alert when an error occurs.
     */
    refresh(event?: any): void {
        this.loading = [];
        let pullList: PullToRefresh;
        if (event) {
            pullList = <PullToRefresh>event.object;
        }

        this.activityService.getActivities()
            .subscribe({
                next: (act: Activity) => this.loading.push(act),
                error: err => {
                    this.loading = undefined;
                    if (pullList) {
                        pullList.refreshing = false;
                    }
                },
                complete: () => {
                    this.activities = this.loading.sort((a: Activity, b: Activity) => {
                        // sort alphabetical to id
                        switch (true) {
                            case a.id < b.id:
                                return -1;
                            case a.id > b.id:
                                return 1;
                            default:
                                return 0;
                        }
                    });

                    this.loading = undefined;
                    if (pullList) {
                        pullList.refreshing = false;
                    }
                }
            });
    }

    /**
     * Open an Activity's URL in the phone's browser.
     * @param id the ID of the Activity
     */
    onURLButtonTap(id: string): void {
        let srv = ServerService.getAddress();

        if (srv) {
            openUrl(`${srv}/activity/${id}/url`);
        } else {
            alert({
                title: "URL unavailable",
                message: "The URL of this activity cannot be opened right now."
            });
        }
    }
}