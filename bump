#!/bin/bash
###################################################################################################
##                                                                                               ##
##   BUMP for {N}                                                                                ##
##  ==============                                                                               ##
##  This script searches in the current directory for package.json and the subdirectory thereof  ##  
##  app/App_Resources/ for the files Android/AndroidManifest.xml and iOS/Info.plist.             ##
##                                                                                               ##
##  If all are found, the version tags and codes in these files are compared and the major,      ##
##  minor or patch version is incremented if they are the same across the three files. The       ##
##  version code is incremented as well.                                                         ##
##                                                                                               ##
##  Should different version numbers be found, the versions can be increased by supplying the    ##
##  version tag manually, in which case the highest version code is incremented and used, and    ##
##  confirmation is required.                                                                    ##
##                                                                                               ##
###################################################################################################


#################
##             ##
##  VARIABLES  ##
##             ##
#################


unset VT_JSON VT_ANDROID VC_ANDROID VT_IOS VC_IOS VC_NEW VT_NEW

VERSION_REGEX=^v?[0-9]+\.[0-9]+\.[0-9]+$
pushd `dirname $0`
JSON_PATH=package.json
ANDROID_PATH=app/App_Resources/Android/AndroidManifest.xml
IOS_PATH=app/App_Resources/iOS/Info.plist


#################
##             ##
##  FUNCTIONS  ##
##             ##
#################


##
# Display usage information and exit.
##
function usage {
    echo "Usage: `basename $0` <version-tag> | major | minor | patch | help

       Updates the full version to the given tag, or increments the major, minor or patch version.
       The version code is always incremented. If a version tag is given, which is not valid, no
       changes will be made.

       The following version tags and codes are updated:
        * package.json (tag only)
        * AndroidManifest.xml
        * Info.plist
        * a new git tag for the version is made and pushed"

        if [[ $# -ne 1 || ! $1 =~ ^help$ ]]; then
            exit 1
        else
            exit 0
        fi
}

##
# Set VT_JSON to the version tag in package.json.
##
function extract_json {
    VT_JSON=`jq -r ".version" $JSON_PATH`
    return 0
}

##
# Set VC_ANDROID and VT_ANDROID to the version code and tag in AndroidManifest.xml.
##
function extract_android {
    set -- `cat $ANDROID_PATH | egrep "android:version(Code|Name)" | sort | sed 's/.*="//;s/".*//'`

    if [[ $# -eq 2 ]]; then
        VC_ANDROID=$1
        VT_ANDROID=$2
        return 0
    else
        return 1
    fi
}

##
# Set VC_IOS and VT_IOS to the version code and tag in Info.plist.
##
function extract_ios {
    pattern="s/.*<string>//g;s/<\/string>*$//"
    VT_IOS=`cat $IOS_PATH | grep -A1 "CFBundleShortVersionString" | grep -v "<key>" | sed "$pattern"`
    VC_IOS=`cat $IOS_PATH | grep -A1 "CFBundleVersion" | grep -v "<key>" | sed "$pattern"`

    if [[ -n "$VT_IOS" && -n "$VC_IOS" ]]; then
        return 0
    else
        return 1
    fi
}

##
# Return the highest version tag of a list.
##
function highest_version {
    OLD=$IFS
    IFS=$'\n'

    args=("$@")
    sorted=(`sort -t. -s -k 1,1nr -k 2,2nr -k 3,3nr <<< "${args[*]}"`)

    echo "${sorted[0]}"
    IFS=$OLD
}


###################
##               ##
##  MAIN SCRIPT  ##
##               ##
###################


# Welcome message
if [[ $# -ne 1 ]]; then
    usage
elif [[ "$1" =~ $VERSION_REGEX|^major$|^minor$|^patch$ ]]; then
    echo " ++============================================++"
    echo " ||  +--\ |  | /\ /\ +--\   /|   /' /\  | '\   ||"
    echo " ||  |--< |  | | V | +--/  /_|  <   | \ |   >  ||"
    echo " ||  +--/ \--/ |   | |       |   \, |  \/ ,/   ||"
    echo " ++===========Bump for NativeScript============++"
    echo 
    echo " Searching for project version"
    echo "+-----------------------------+"
else
    usage $1
fi

##
# Extract versions from files.
##
echo -n " * "
if [[ -f $JSON_PATH ]]; then
    extract_json &&
    echo "Found package.json:        v${VT_JSON}" ||
    echo Unable to get version info from ${JSON_PATH}
else
    echo Cannot find ${JSON_PATH}
fi

echo -n " * "
if [[ -f $ANDROID_PATH ]]; then
    extract_android &&
    echo "Found AndroidManifest.xml: v${VT_ANDROID} (${VC_ANDROID})" ||
    echo Unable to get version info from ${ANDROID_PATH}
else
    echo Cannot find ${ANDROID_PATH}
fi

echo -n " * "
if [[ -f $IOS_PATH ]]; then
    extract_ios &&
    echo "Found Info.plist:          v${VT_IOS} (${VC_IOS})" ||
    echo Unable to get version info from ${IOS_PATH}
else
    echo Cannot find ${IOS_PATH}
fi

echo

if [[ -z ${VT_JSON+x}
   || -z ${VT_ANDROID+x} || -z ${VC_ANDROID+x}
   || -z ${VT_IOS+x}     || -z ${VC_IOS+x} ]]; then
    echo All required files were not found, aborting!
    exit 2
fi

##
# Calculate new version code and tag.
##
echo " Determining new version"
echo "+-----------------------+"

if [[ $1 =~ $VERSION_REGEX ]]; then
    # check and set given version tag
    VT_NEW=`echo $1 | sed 's/^v//'`
    echo " * Checking height of new version tag"
    HIGHEST_OLD=`highest_version $VT_JSON $VT_ANDROID $VT_IOS`
 
    if [[ $HIGHEST_OLD == $VT_NEW || `highest_version $HIGHEST_OLD $VT_NEW` != $VT_NEW ]]; then
        echo
        echo New version is not higher than all old versions, aborting!
        exit 3
    fi

    echo " * Checking version uniformity"
    if [[ $VT_JSON != $VT_ANDROID || $VT_JSON != $VT_IOS
       || $VC_ANDROID != $VC_IOS ]]; then
        read -p " --> Versions do not match, are you sure you want to align them to v$VT_NEW?
 Y/N " -n 1 -r
        echo

        if [[ ! $REPLY =~ ^[yY]$ ]]; then
            echo "     That's all then. Bye!"
            exit 0
        fi
    fi

    VC_NEW=`highest_version $VC_ANDROID $VC_IOS`
    ((VC_NEW++))
else
    # increment versions
    echo " * Checking version uniformity"
    if [[ $VT_JSON != $VT_ANDROID || $VT_JSON != $VT_IOS
       || $VC_ANDROID != $VC_IOS ]]; then
        echo The versions are out of sync. Automatically bumping the version is not\
             possible. Provide a new version tag or correct the versions manually.
        exit 4
    fi

    echo " * Calculating new version tag"

    OLD_IFS=$IFS
    IFS="."
    VERSION_TAG=()
    read -ra VERSION_TAG <<< "$VT_JSON"

    case $1 in
        major)
            ((VERSION_TAG[0]++))
            ((VERSION_TAG[1]=0))
            ((VERSION_TAG[2]=0))
            ;;
        minor)
            ((VERSION_TAG[1]++))
            ((VERSION_TAG[2]=0))
            ;;
        patch)
            ((VERSION_TAG[2]++))
            ;;
    esac

    VT_NEW="${VERSION_TAG[*]}"
    IFS=$OLD_IFS
    
    VC_NEW=$VC_ANDROID
    ((VC_NEW++))
fi

echo " * New version: v$VT_NEW ($VC_NEW)"
echo

##
# Write new versions to files.
##
echo " Setting new version"
echo "+-------------------+"

# package.json
mv $JSON_PATH $JSON_PATH.old
new_tag="$VT_NEW" jq '.version=env.new_tag' $JSON_PATH.old > $JSON_PATH 2> /dev/null
JSON_RESULT=$?

if [[ $JSON_RESULT -eq 0 ]]; then
    echo " * Updated package.json"
else
    rm -f $JSON_PATH.tmp
    echo Failed to update package.json, aborting!
    exit 5
fi

# AndroidManifest.xml
sed -e "s/\(android:versionName=\"\)[0-9\.]\+\"/\1$VT_NEW\"/" \
    -e "s/\(android:versionCode=\"\)[0-9]\+\"/\1$VC_NEW\"/" \
    -i.old $ANDROID_PATH

ANDROID_RESULT=$?

if [[ $ANDROID_RESULT -eq 0 ]]; then
    echo " * Updated AndroidManifest.xml"
else
    echo Failed to update AndroidManifest.xml, aborting!

    if [[ -f $ANDROID_PATH.old ]]; then
        mv $ANDROID_PATH.old $ANDROID_PATH
    fi

    mv $JSON_PATH.old $JSON_PATH

    exit 6
fi

# Info.plist
sed -e "/CFBundleVersion/{
            N
            s/\(<string>\).\+\(<\/string>\)/\1$VC_NEW\2/
        }" \
    -e "/CFBundleShortVersionString/{
            N
            s/\(<string>\).\+\(<\/string>\)/\1$VT_NEW\2/
        }" \
    -i.old $IOS_PATH

IOS_RESULT=$?

if [[ $IOS_RESULT -eq 0 ]]; then
    echo " * Updated Info.plist"
else
    echo Failed to update Info.plist, aborting!

    if [[ -f $IOS_PATH.old ]]; then
        mv $IOS_PATH.old $IOS_PATH
    fi

    mv $JSON_PATH.old $JSON_PATH
    mv $ANDROID_PATH.old $ANDROID_PATH

    exit 7
fi

# Git repo
read -p " --> Commit with message 'version bump', tag and push to v$VT_NEW at origin?
 Y/N " -n 1 -r
echo

if [[ $REPLY =~ ^[yY]$ ]]; then
    git add $JSON_PATH $ANDROID_PATH $IOS_PATH
    git commit -m "version bump" > /dev/null
    git tag v$VT_NEW > /dev/null
    git push origin v$VT_NEW > /dev/null

    GIT_RESULT=$?
    
    if [[ $GIT_RESULT -eq 0 ]]; then
        echo " * Committed, tagged and pushed new version"
    else
        echo "Failed to update repository, aborting!"
        echo "Git reports:"
        git status
    fi
fi


##
# Ask whether to remove the backed up files.
##
read -p " --> Remove backed up versions of package.json, AndroidManifest.xml and Info.plist?
 Y/N " -n 1 -r
echo

if [[ $REPLY =~ ^[yY]$ ]]; then
    rm -f $JSON_PATH.old $ANDROID_PATH.old $IOS_PATH.old
fi

echo "-+-  All done! -+-"

