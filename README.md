# `ptt-mobile` repo

The Presence Tracking Tool (PTT) is a tool which can be used
to track the occupation of people across different activities.
The tool has been developed for use at the
[JOTI-hunt](http://www.jotihunt.nl), but its use is not limited to that.

Ptt-mobile is a NativeScript mobile app, and one of the three composing parts
of the Presence Tracking Tool, which consists of a storage, a server and a
cross-platform mobile app for Android/iOS.

The mobile app is written in TypeScript and uses the Angular 2 NativeScript
framework to make a cross-platform mobile application. The app can be built
for either Android or iOS from mostly a single code base. Certain
functionality requires platform specific code, but this is kept at a minimum.

The major and minor versioning is in sync ptt-server, which means the app's
functionality corresponds to that of ptt-server of the same version. The app
can read and manipulate the following, through communication with the server:

* persons (from v0.1)
* activities (from v0.2)

#### Network flow
The network traffic in the app is routed as seen in the diagram below:

![Network diagram](img-src/doc/Network flow.png)

Diagram made at [draw.io](https://www.draw.io).

